#
# Be sure to run `pod lib lint ios-teragence-sdk-background.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
s.name             = 'ios-teragence-sdk-background'
s.version          = '1.2.5.14'
s.summary          = 'This is short description of ios-teragence-sdk-background.'
s.description      = 'This description is used to generate tags and improve search results.'

s.homepage         = 'https://bitbucket.org/teragence/ios-sdk-background'
# s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
s.license          = { :type => 'EULA', :file => 'LICENSE' }
s.author           = { 'Teragence' => 'info@teragence.com' }
s.source           = { :git => 'https://bitbucket.org/teragence/ios-sdk-background', :tag => s.version.to_s }
s.library = 'xml2'
other_ldflags = '-ObjC' + '-framework' + '-lz'  + '-lstdc++'
s.xcconfig     = {

'HEADER_SEARCH_PATHS' => '$(SDKROOT)/usr/include/libxml2'

#"OTHER_LDFLAGS"  => other_ldflags
}
s.requires_arc = true
s.social_media_url = 'http://www.teragence.com'
s.ios.deployment_target = '8.0'
s.vendored_frameworks = 'SDK_Teragence.framework'
s.frameworks = 'Foundation', 'CoreLocation'
s.resource_bundles = {

}
s.source_files = 'SDK_Teragence.framework/Headers/*.h'
s.public_header_files = 'SDK_Teragence.framework/Headers/*.h'

end
